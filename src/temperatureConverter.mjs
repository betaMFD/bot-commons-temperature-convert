// Description:
//   convert temperatures when they've been said randomly in chat

export const temperatureConvert = {
  regex: /(^|\s)\-?\d+\.?\d*(\s|o|°|\*)?[FCK][,.;\?'"]?($|\s)/i,
  tag_bot: false,
  execute: function(message) {
    const content = message.content;
    let temp = content.match(/\-?\d+\.?\d*.?[FCK]/i)[0];
    let theNumber = temp.match(/\-?\d+\.?\d*/)[0]
    let theType = temp.match(/[FCK]/i)[0]
    if (theType === 'F' || theType === 'f') {
      let celsius = (theNumber - 32) * 5/9;
      celsius = Math.round((celsius + Number.EPSILON) * 100) / 100
      message.reply(theNumber + '°F is ' + celsius + '°C');
    } else if (theType === 'C' || theType === 'c') {
      let fahrenheit = (theNumber * 9/5) + 32;
      fahrenheit = Math.round((fahrenheit + Number.EPSILON) * 100) / 100
      message.reply(theNumber + '°C is ' + fahrenheit + '°F');
    } else if (theType === 'K' || theType === 'k') {
      //because every smartass who sees this script run tries kelvin
      let celsius = theNumber - 273.15;
      celsius = Math.round((celsius + Number.EPSILON) * 100) / 100
      let fahrenheit = (theNumber - 273.15) * 9 / 5 + 32;
      fahrenheit = Math.round((fahrenheit + Number.EPSILON) * 100) / 100
      message.reply(theNumber + 'K is ' + fahrenheit + '°F or ' + celsius + '°C');
    }
  }
}
